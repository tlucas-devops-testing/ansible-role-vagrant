# Manage Vagrant hosts via Ansible

This is an [Ansible Role][role] for managing [Vagrant][vagrant] installations
on a host.

[role]: https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html
[vagrant]: https://www.vagrantup.com/
